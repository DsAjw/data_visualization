from plotly.graph_objs import Bar, Layout
from plotly import offline


from die import Die

# Create 3 D6 dice
die1 = Die()
die2 = Die()
die3 = Die()
# Make some rolls and store results into a list
results = [die1.roll() + die2.roll() + die3.roll() for result in range(1000)]

# Analyze the results
max_result = die1.num_sides + die2.num_sides + die3.num_sides
frequencies = [results.count(value) for value in range(3,max_result+1)]

# Visualize the results
x_values = list(range(3,max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title':'Result', 'dtick': 1} #dtick controls spacing between tick marks on the axis
y_axis_config = {'title': 'Frequency of Result'}
my_layout = Layout(title='Results of rolling three D6 1000 times',
        xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data':data, 'layout': my_layout}, filename='d6_d6_d6.html')