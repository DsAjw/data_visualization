import csv
from datetime import datetime
import matplotlib.pyplot as plt

filename = 'data/death_valley_2018_simple.csv'
with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    for index, column_header in enumerate(header_row):
        print(index, column_header)

    # Get dates and high and low temperatures from this file
    dates, highs, lows = [], [], []
    for row in reader: # the loop starts from second line of the file since we read the first line above using next()
        current_date = datetime.strptime(row[2], '%Y-%m-%d') # grabs the date from index 2 and formats it in YYYY-MM-DD
        try:
            high = int(row[4])
            low = int(row[5])
        except ValueError: # error handling code to check for missing data
            print(f"Missing data for {current_date}")
        else:
            dates.append(current_date)
            highs.append(high)
            lows.append(low)
    
    # Plot the high and low temperatures
    plt.style.use('seaborn')
    fig, ax = plt.subplots()
    ax.plot(dates, highs, c='red', alpha=0.5)
    ax.plot(dates, lows, c='blue', alpha=0.5)
    plt.fill_between(dates, highs, lows, facecolor = 'blue', alpha=0.1) # this shades the region between the 2 plots

    # Format plot
    plt.title("Daily high and low temperatures, 2018\n Death Valley, CA", fontsize=20)
    plt.xlabel('',fontsize=16)
    fig.autofmt_xdate() # draws the dates diagonally to prevent overlapping
    plt.ylabel("Temperature (F)", fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=16)

    plt.show()