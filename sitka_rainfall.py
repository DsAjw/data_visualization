import csv
from datetime import datetime
import matplotlib.pyplot as plt

filename = 'data/sitka_weather_2018_simple.csv'
with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    for index, column_header in enumerate(header_row): # enumerate returns both the index and value of each item looped through
        print(index, column_header)
    
    dates, rains = [], []
    for row in reader:
        current_date = datetime.strptime(row[2], '%Y-%m-%d')
        try:
            rain = row[3]
        except ValueError:
            print(f"Missing data on {current_date}")
        else:
            dates.append(current_date)
            rains.append(rain)
    
    # Plot the high and low temperatures
    plt.style.use('seaborn')
    fig, ax = plt.subplots(figsize=(15,9), dpi=128) #figsize adjusts the size of the whole figure
    ax.plot(dates, rains, c='blue')

    # Format plot
    plt.title("Daily rainfall, 2018\n Sitka", fontsize=20)
    plt.xlabel('',fontsize=16)
    fig.autofmt_xdate() # draws the dates diagonally to prevent overlapping
    plt.ylabel("Rainfall (in)", fontsize=16)
    plt.tick_params(axis='y', which='major', labelsize=5)

    plt.show()
