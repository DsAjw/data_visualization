import csv

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline
from plotly import colors

filename = 'data/world_fires_1_day.csv'
with open(filename) as f:
    reader = csv.reader(f) # this creates a reader object associated with the file and assign to reader
    header_row = next(reader) # this returns the first line of the file that has the file headers

    lat_index = 0
    lon_index = 0
    brightness_index = 0
    for index, column_header in enumerate(header_row): # enumerate returns both the index and value of each item looped through
        if column_header == 'latitude':
            lat_index = index
        if column_header == 'longitude':
            lon_index = index
        if column_header == 'brightness':
            brightness_index = index

    lats, lons, brightness = [],[],[]
    for row in reader:
        lats.append(row[lat_index])
        lons.append(row[lon_index])
        brightness.append(float(row[brightness_index]))

# Map the fires by building a world map
# Scattergeo object created in a list because we can plot > 1 data set on any visualization
# data = [Scattergeo(lon=lons, lat=lats)] # simplest way to define data but not the best if we want to customize
data = [{
    'type' : 'scattergeo',
    'lon' : lons,
    'lat' : lats,
    'marker' : {
        # change the settings of the markers
        'color' : brightness,
        'colorscale' : 'Hot', # scale from dark blue to bright yellow
        'reversescale' : True, # reverse it so yellow are small fires, dark red for big fires
        'colorbar' : {'title' : 'Brightness'},
        },
    }]
my_layout = Layout(title="Global Fires over a 1-day period")

fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='global_fires.html')
