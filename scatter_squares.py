import matplotlib.pyplot as plt

x_values = range(1,1001) # a list containing numbers 1 to 1000
y_values = [x**2 for x in x_values] # a list containing square of each element in x_values

plt.style.use('seaborn')
fig, ax = plt.subplots()

# 1st element in x_values will be plotted by 1st element in y_values and so on
# so we are plotting (1,1) (2,4) (3,9) (4, 16) (5,25) and so on
#ax.scatter(x_values,y_values, c='red', s=10) # s sets the size of the dots, c for color
#ax.scatter(x_values,y_values, c=(0,0.8,0), s=10) # using rgb values is another way but they are 0 (black) 1(white)
# using colormap to have a gradient
ax.scatter(x_values,y_values, c=y_values, cmap=plt.cm.Blues, s=10)



# Set chart title and label axes
ax.set_title("Square Numbers", fontsize=24)
ax.set_xlabel("Value", fontsize=14)
ax.set_ylabel("Square of Value", fontsize=14)

# Set size of tick labels
ax.tick_params(axis='both', which='major', labelsize=14) # this method sets the size of the numbers at each tick

# Set the range for x and y axis
ax.axis([0,1100, 0,1100000]) # 0,1100 for x, 0,1100000 for y

#plt.show()

# Save the graph into a file
plt.savefig('squares_plot.png', bbox_inches = 'tight') 
# 1st argument = filename, 2nd removes whitespace around the graph
# if still want the whitespace, just omit the 2nd argument
