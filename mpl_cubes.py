import matplotlib.pyplot as plt

x_values = range(1,5001)
y_values = [x**3 for x in x_values]

plt.style.use('dark_background')
fig, ax = plt.subplots()

ax.scatter(x_values,y_values, c=y_values, cmap=plt.cm.Reds, s=1)

# Set chart title and label axes
ax.set_title("Cube Numbers", fontsize=24)
ax.set_xlabel("Value", fontsize=14)
ax.set_ylabel("Cube of Value", fontsize=14)

# Set size of tick labels
ax.tick_params(axis='both', which='major', labelsize=14) # this method sets the size of the numbers at each tick

# Set the range for x and y axis
ax.axis([1,5000, 1,125_000_000_000]) # 0,1100 for x, 0,1100000 for y

plt.show()