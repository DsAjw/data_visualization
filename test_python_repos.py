import unittest
from python_repos import _get_status_code

class TestPythonRepos(unittest.TestCase):
    """Tests for 'python_repos.py' """

    # test case for calls without populations
    def test_api_call(self):
        """Make sure that the API call was successful"""
        url = 'https://api.github.com/search/repositories?q=language:python&sort=stars'
        headers = {'Accept': 'application/vnd.github.v3+json'}
        status_code = _get_status_code(url,headers)
        self.assertEqual(status_code, 200)

if __name__ == '__main__':
    unittest.main()