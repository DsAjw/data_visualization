from plotly.graph_objs import Bar, Layout
from plotly import offline


from die import Die

# Create a D6 and D10
die1 = Die()
die2 = Die(10)

# Make some rolls and store results into a list
results = [die1.roll() + die2.roll() for result in range(50000)]

# Analyze the results
max_result = die1.num_sides + die2.num_sides
frequencies = [results.count(value) for value in range(2,max_result+1)]

# Visualize the results
x_values = list(range(2,max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title':'Result', 'dtick': 1} #dtick controls spacing between tick marks on the axis
y_axis_config = {'title': 'Frequency of Result'}
my_layout = Layout(title='Results of rolling a D6 and a D10 50000 times',
        xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data':data, 'layout': my_layout}, filename='d6_d10.html')