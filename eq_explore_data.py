import json

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline

# Explore the structure of the data
filename = 'data/eq_data_1_day_m1.json' # file for 1 day worth of earthquakes
with open(filename) as f:
    all_eq_data = json.load(f)

# # This creates a more readable form of the above json file
# readable_file = 'data/readable_eq_data.json'
# with open(readable_file, 'w') as f:
#     json.dump(all_eq_data,f,indent=4)

all_eq_dicts = all_eq_data['features']
print(len(all_eq_dicts))

# Extracting magnitudes, longitudes and latitdudes
mags, lons, lats = [],[],[]
for eq_dict in all_eq_dicts:
    mag = eq_dict['properties']['mag']
    lon = eq_dict['geometry']['coordinates'][0]
    lat = eq_dict['geometry']['coordinates'][1]
    mags.append(mag)
    lons.append(lon)
    lats.append(lat)

print(mags[:10])
print(lons[:5])
print(lats[:5])

# Map the earthquakes, by building a world map
# Scattergeo object created in a list because we can plot > 1 data set on any visualization
# data = [Scattergeo(lon=lons, lat=lats)] # simplest way to define data but not the best if we want to customize
data = [{
    'type' : 'scattergeo',
    'lon' : lons,
    'lat' : lats,
    'marker' : {
        # change the settings of the markers
        'size' : [5*mag for mag in mags],
        },
    }]
my_layout = Layout(title='Global Earthquakes')

fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='global_earthquakes.html')