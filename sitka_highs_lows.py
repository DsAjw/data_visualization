import csv # csv module to parse csv file headers
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
filename = 'data/sitka_weather_2018_simple.csv'
with open(filename) as f:
    reader = csv.reader(f) # this creates a reader object associated with the file and assign to reader
    header_row = next(reader) # this returns the first line of the file that has the file headers

    TMAX_index = 0
    TMIN_index = 0
    DATE_index = 0
    STATION_index = 0
    for index, column_header in enumerate(header_row): # enumerate returns both the index and value of each item looped through
        if column_header == 'TMAX':
            TMAX_index = index
        if column_header == 'TMIN':
            TMIN_index = index
        if column_header == 'NAME':
            STATION_index = index
        if column_header == 'DATE':
            DATE_index = index

    # Get dates and high and low temperatures from this file
    dates, highs, lows = [], [], []
    name = ''
    for row in reader: # the loop starts from second line of the file since we read the first line above using next()
        current_date = datetime.strptime(row[DATE_index], '%Y-%m-%d') # grabs the date from index 2 and formats it in YYYY-MM-DD
        high = int(row[TMAX_index]) # index 5 corresponds to TMAX which is what we want, we convert to int from string so we can use it
        low = int(row[TMIN_index])
        name = row[STATION_index]
        dates.append(current_date)
        highs.append(high)
        lows.append(low)
    
    # Plot the high and low temperatures
    plt.style.use('seaborn')
    fig, ax = plt.subplots()
    ax.plot(dates, highs, c='red', alpha=0.5)
    ax.plot(dates, lows, c='blue', alpha=0.5)
    plt.fill_between(dates, highs, lows, facecolor = 'blue', alpha=0.1) # this shades the region between the 2 plots

    # Format plot
    plt.title(f"Daily high and low temperatures, 2018\n{name}", fontsize=24)
    plt.xlabel('',fontsize=16)
    fig.autofmt_xdate() # draws the dates diagonally to prevent overlapping
    plt.ylabel("Temperature (F)", fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=16)

    # Set the y axis settings
    axes = plt.gca()
    axes.set_ylim([20,120])
    #plt.yticks(np.arange(0,121,40))
    plt.show()