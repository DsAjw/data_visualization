import json

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline
from plotly import colors

# Available colorscales in plotly
for key in colors.PLOTLY_SCALES.keys():
    print(key)

# Explore the structure of the data
#filename = 'data/eq_data_1_day_m1.json' # file for 1 day worth of earthquakes
filename = 'data/eq_data_30_day_m1.json'
with open(filename) as f:
    all_eq_data = json.load(f)

# # This creates a more readable form of the above json file
# readable_file = 'data/readable_eq_data.json'
# with open(readable_file, 'w') as f:
#     json.dump(all_eq_data,f,indent=4)

all_eq_dicts = all_eq_data['features']

# grab title from the metadata
my_title = all_eq_data['metadata']['title']

# Extracting magnitudes, longitudes and latitdudes
# add hover text as well
mags, lons, lats, hover_texts = [],[],[],[]
for eq_dict in all_eq_dicts:
    mags.append(eq_dict['properties']['mag'])
    lons.append(eq_dict['geometry']['coordinates'][0])
    lats.append(eq_dict['geometry']['coordinates'][1])
    hover_texts.append(eq_dict['properties']['title'])

# Map the earthquakes, by building a world map
# Scattergeo object created in a list because we can plot > 1 data set on any visualization
# data = [Scattergeo(lon=lons, lat=lats)] # simplest way to define data but not the best if we want to customize
data = [{
    'type' : 'scattergeo',
    'lon' : lons,
    'lat' : lats,
    'text' : hover_texts,
    'marker' : {
        # change the settings of the markers
        'size' : [5*mag for mag in mags],
        'color' : mags,
        'colorscale' : 'Viridis', # scale from dark blue to bright yellow
        'reversescale' : True, # reverse it so yellow are small earthquakes, dark blue for big earthquakes
        'colorbar' : {'title' : 'Magnitude'},
        },
    }]
my_layout = Layout(title=my_title)
print(mags)
fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='global_earthquakes.html')