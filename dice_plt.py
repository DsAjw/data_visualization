import matplotlib.pyplot as plt

from die import Die

# Make a D6
die = Die()

# Roll 1000 times
results = [die.roll() for value in range(1000)]
frequencies = [results.count(value) for value in range(1,die.num_sides+1)]
print(frequencies)
# Plot the points
plt.style.use('classic')
fig, ax = plt.subplots(figsize=(15,9), dpi=128) #figsize adjusts the size of the whole figure

ax.set_title("1000 single D6 rolls", fontsize=24)
ax.set_xlabel("Result", fontsize=14)
ax.set_ylabel("Frequency", fontsize=14)

x_values = range(1,die.num_sides+1)
ax.scatter(x_values,frequencies, s=25)


# Set the range for x and y axis
ax.axis([1,6, 1,250])

plt.show()
